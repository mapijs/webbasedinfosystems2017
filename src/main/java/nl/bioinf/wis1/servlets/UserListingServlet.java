package nl.bioinf.wis1.servlets;

import nl.bioinf.wis1.messages.MessageFactory;
import nl.bioinf.wis1.users.Address;
import nl.bioinf.wis1.users.User;
import sun.misc.Request;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UserListingServlet", urlPatterns = "/list.users")
public class UserListingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        User numberOne = createUserOne();
        request.setAttribute("userOne", numberOne);
        RequestDispatcher requestDispatcher = request.getRequestDispatcher("UserListing.jsp");
        requestDispatcher.forward(request, response);
    }

    private User createUserOne() {
        User one = new User("Henk", "Henk");
        one.setEmail("Henk@example.com");
        Address address = new Address(7, "a", "9701 DA", "Zernikeplein", "Groningen");
        one.setAddress(address);
        return one;
    }
}
